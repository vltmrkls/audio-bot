//
//  AppDelegate.swift
//  Audio Bot
//
//  Created by Klaus Voltmer on 3/22/19.
//  Copyright © 2019 Voltmer Systems. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

